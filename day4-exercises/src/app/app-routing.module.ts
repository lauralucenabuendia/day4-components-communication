import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'expandable',
    pathMatch: 'full'
  },
  {
    path: 'expandable',
    loadChildren: () => import('./pages/expandable/expandable.module').then( m => m.ExpandablePageModule)
  },
  {
    path: 'rating',
    loadChildren: () => import('./pages/rating/rating.module').then( m => m.RatingPageModule)
  },
  {
    path: 'fruit-list',
    loadChildren: () => import('./pages/fruit-list/fruit-list.module').then( m => m.FruitListPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
