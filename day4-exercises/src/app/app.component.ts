import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Expandable', url: '/expandable', icon: 'expand' },
    { title: 'Rating', url: '/rating', icon: 'star-half' },
    { title: 'Fruit list', url: '/fruit-list', icon: 'nutrition' }
  ];

  constructor() {}
}
