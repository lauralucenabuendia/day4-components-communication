import { Component, Input } from '@angular/core';
import { Expandable } from '../../models/interfaces';

@Component({
  selector: 'app-expandable-component',
  templateUrl: './expandable.component.html',
  styleUrls: ['./expandable.component.scss'],
})
export class ExpandableComponent{
  @Input() title: string;
  public displayedInfo: boolean;

  constructor() { }

  showInformationToggle(){
    this.displayedInfo = !this.displayedInfo;
  }

}
