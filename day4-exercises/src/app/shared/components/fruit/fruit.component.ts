import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Fruit } from '../../models/interfaces';

@Component({
  selector: 'app-fruit-component',
  templateUrl: './fruit.component.html',
  styleUrls: ['./fruit.component.scss'],
})
export class FruitComponent{
  @Input() fruit: Fruit;

  constructor(private router: Router) { }

  goToDetails(){
    this.router.navigateByUrl(`/fruit-list/${this.fruit.id}`);
  }

}
