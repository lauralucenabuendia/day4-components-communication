import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-rating-component',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
})
export class RatingComponent implements OnInit {
  @Output() puntuations = new EventEmitter<number[]>();

  public positivePuntuation: number;
  public badPuntuation: number;
  public puntuationsArr: number[];

  public averageScore: string;
  private totalScore: number;
  private numberVotes: number;

  constructor() { }

  ngOnInit() {
    this.puntuationsArr = [0, 0];
    this.resetData();
  }

  addPositivePuntuation(){
    this.positivePuntuation += 1;
    this.totalScore += 10;
    this.numberVotes++;

    this.puntuationsArr[0] += 1;
    console.log(this.puntuationsArr)
    this.calculateAveragScoreAndSendPuntuations();
  }


  addBadPuntuation(){
    this.badPuntuation += 1;
    this.numberVotes++;

    this.puntuationsArr[1] += 1;
    console.log(this.puntuationsArr)
    this.calculateAveragScoreAndSendPuntuations();
  }


  calculateAveragScoreAndSendPuntuations(){
    this.puntuations.emit(this.puntuationsArr);
    this.puntuationsArr.fill(0);
    this.averageScore = (this.totalScore / this.numberVotes).toFixed(2);
  }

  resetData(){
    this.positivePuntuation = 0;
    this.badPuntuation = 0;
    this.totalScore = 0;
    this.averageScore = '0.0';
    this.numberVotes = 0;
  }

}
