import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

// Componentes del módulo
import { ExpandableComponent } from '../shared/components/expandable/expandable.component';
import { FruitComponent } from '../shared/components/fruit/fruit.component';
import { RatingComponent } from '../shared/components/rating/rating.component';



@NgModule({
  declarations: [ExpandableComponent, FruitComponent, RatingComponent],
  imports: [ CommonModule, IonicModule, FormsModule ],
  exports: [CommonModule, IonicModule, FormsModule, ExpandableComponent, FruitComponent, RatingComponent]
})
export class SharedModule { }
