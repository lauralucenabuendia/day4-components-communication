import { Injectable } from '@angular/core';
import { Fruit } from '../models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class FruitsService {
  private fruitList: Fruit[];

  constructor() {
    this.fruitList = [
      {
        id: 1,
        name: 'Apple',
        image: '../../../../assets/images/fruits/apple.png',
        description: 'The usually round red, green, or yellow fruit of a small tree of the rose family.',
        link: 'https://www.wikipedia.org/wiki/Apple'
      },
      {
        id: 2,
        name: 'Orange',
        image: '../../../../assets/images/fruits/orange.png',
        description: 'A rounded, reddish yellow, bitter or sweet citrus fruit that can be eaten.',
      },
      {
        id: 3,
        name: 'Lemon',
        image: '../../../../assets/images/fruits/lemon.png',
        description: 'The yellowish, acid fruit of a subtropical citrus tree.',
        link: 'https://www.wikipedia.org/wiki/Lemon'
      },
      {
        id: 4,
        name: 'Pear',
        image: '../../../../assets/images/fruits/pear.png',
        description: 'The rounded fruit of a tree of the rose family.',
      },
      {
        id: 5,
        name: 'Strawberry',
        image: '../../../../assets/images/fruits/strawberry.png',
        description: 'The fleshy red fruit of a stemless plant belonging to the rose family.',
        link: 'https://www.wikipedia.org/wiki/Strawberry'
      },
      {
        id: 6,
        name: 'Pineapple',
        image: '../../../../assets/images/fruits/pineapple.png',
        description: 'A tropical plant having a short stem and rigid, spiny leaves.',
      },
      {
        id: 7,
        name: 'Watermelon',
        image: '../../../../assets/images/fruits/watermelon.png',
        description: 'A large melon with a hard, green rind and sweet, juicy, usually red pulp.',
        link: 'https://www.wikipedia.org/wiki/Watermelon'
      },
      {
        id: 8,
        name: 'Grapes',
        image: '../../../../assets/images/fruits/grapes.png',
        description: 'The smooth-skinned, green or purple fruit that grows in clusters on vines, may be eaten, and is used to make wine.',
      }
    ];
  }


  getFruitList(): Fruit []{
    return this.fruitList;
  }

  grtFruitByID(id: number): Fruit{
    return this.fruitList.find((fruit) => fruit.id === id);
  }
}
