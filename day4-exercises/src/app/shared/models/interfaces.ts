// Interfaz para el primer ejercicio
export interface Expandable {
  title: string;
  content: string;
}


// Interfaz para las Frutas
export interface Fruit {
  id: number;
  name: string;
  image: string;
  description: string;
  link?: string;
}
