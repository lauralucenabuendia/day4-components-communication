import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FruitListPageRoutingModule } from './fruit-list-routing.module';
import { FruitListPage } from './fruit-list.page';

@NgModule({
  imports: [SharedModule, FruitListPageRoutingModule],
  declarations: [FruitListPage]
})
export class FruitListPageModule {}
