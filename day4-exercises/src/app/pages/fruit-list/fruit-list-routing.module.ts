import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FruitListPage } from './fruit-list.page';

const routes: Routes = [
  {
    path: '',
    component: FruitListPage,
  },
  {
    path: ':id',
    loadChildren: () =>
      import('../fruit-details/fruit-details.module').then(
        (m) => m.FruitDetailsPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FruitListPageRoutingModule {}
