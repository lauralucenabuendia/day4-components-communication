import { Component, OnInit } from '@angular/core';
import { Fruit } from 'src/app/shared/models/interfaces';
import { FruitsService } from 'src/app/shared/services/fruits.service';

@Component({
  selector: 'app-fruit-list',
  templateUrl: './fruit-list.page.html',
  styleUrls: ['./fruit-list.page.scss'],
})
export class FruitListPage implements OnInit {
  myFruitList: Fruit[];


  // Inyectamos el servicio
  constructor(private fruitServ: FruitsService) { }

  ngOnInit() {
    this.myFruitList = this.fruitServ.getFruitList();
  }
}
