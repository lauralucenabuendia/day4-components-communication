import { Component, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.page.html',
  styleUrls: ['./rating.page.scss'],
})
export class RatingPage implements OnInit {
  puntuationsCounter: number[];
  numberItems: number[];

  constructor() { }

  ngOnInit() {
    this.puntuationsCounter = [0, 0];
    this.numberItems = new Array<number>(4);
  }

  updatePuntuation(puntuation: number[]){
    this.puntuationsCounter = this.puntuationsCounter.map(function (num, idx) {
      return num + puntuation[idx];
    });
  }
}
