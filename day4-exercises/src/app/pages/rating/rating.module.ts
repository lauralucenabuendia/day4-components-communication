import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module'
import { RatingPageRoutingModule } from './rating-routing.module';
import { RatingPage } from './rating.page';

@NgModule({
  imports: [ SharedModule, RatingPageRoutingModule],
  declarations: [RatingPage]
})
export class RatingPageModule {}
