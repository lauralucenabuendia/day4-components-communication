import { Component, OnInit } from '@angular/core';
// Importamos nuestra interfaz expandable
import { Expandable } from 'src/app/shared/models/interfaces';

@Component({
  selector: 'app-expandable',
  templateUrl: './expandable.page.html',
  styleUrls: ['./expandable.page.scss'],
})
export class ExpandablePage implements OnInit {
  expandables : Expandable[];

  constructor() { }

  ngOnInit() {
    this.expandables = [
      { "title": "Title 1",
        "content": "content 1" },

      { "title": "Title 2",
        "content": "content 2" },

      { "title": "Title 3",
        "content": "content 3" },

      { "title": "Title 4",
        "content": "content 4" },

      { "title": "Title 5",
        "content": "content 5" }
    ];
  }

}
