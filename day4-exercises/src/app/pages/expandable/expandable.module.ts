import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { ExpandablePageRoutingModule } from './expandable-routing.module';
import { ExpandablePage } from './expandable.page';

@NgModule({
  imports: [SharedModule, ExpandablePageRoutingModule],
  declarations: [ExpandablePage]
})
export class ExpandablePageModule {}
