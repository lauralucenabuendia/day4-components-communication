import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Fruit } from 'src/app/shared/models/interfaces';
import { FruitsService } from 'src/app/shared/services/fruits.service';

@Component({
  selector: 'app-fruit-details',
  templateUrl: './fruit-details.page.html',
  styleUrls: ['./fruit-details.page.scss'],
})
export class FruitDetailsPage implements OnInit {
  fruit: Fruit;

  constructor(private actRoute: ActivatedRoute,
    private fruitServ: FruitsService) { }

  ngOnInit() {
    const id: number = +this.actRoute.snapshot.paramMap.get('id');
    this.fruit = this.fruitServ.grtFruitByID(id);
  }
}
